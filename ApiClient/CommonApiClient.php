<?php

namespace App\Services\RollsClient;

use App\Services\RollsClient\Exceptions\CommonApiValidationException;

/**
 *
 * @package App\Services\RollsClient
 */
class CommonApiClient
{

    const BASE_URI = 'rolls_uri';

    /**
     * @var string
     */
    private $appKey;

    /**
     * @var string
     */
    private $appPwd;

    /**
     * Kantoor ID - for dutch
     * @var string
     */
    private $serviceID;

    /**
     * @var \Exception[]
     */
    private $errorBag = [];

    /**
     * @param string $appKey
     * @param string $appPwd
     * @param string $serviceID
     */
    public function __construct(string $appKey, string $appPwd, string $serviceID)
    {
        $this->appKey = $appKey;
        $this->appPwd = $appPwd;
        $this->serviceID = $serviceID;
    }

    /**
     * @param  string        $envelope       The main part of request in XML format. Rolls api calls it envelope.
     * @return \SimpleXMLElement             Response body.
     */
    public function send(string $envelope): \SimpleXMLElement
    {
        $envelope = trim(preg_replace('/\s\s+/', ' ', $envelope));
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::BASE_URI);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $this->getRequestBody($envelope));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $out = curl_exec($curl);
        curl_close($curl);
        $response = new \SimpleXMLElement($out);
        $this->checkErrors($response);
        return $response;
    }

    /**
     * @return \Exception[]
     */
    public function getErrors(): array
    {
        return $this->errorBag;
    }

    /**
     * @param  string $envelope
     * @return string
     */
    private function getRequestBody(string $envelope)
    {
        return http_build_query([
            'FunctieXML' => $envelope,
            'ClientAppKey' => $this->appKey,
            'ClientAppPassword' => $this->appPwd,
            'RollsProductID' => '',
            'KantoorID' => $this->serviceID,
            'TimeoutSeconds' => '',
            'Test' => config('rolls.test_mode'),
        ]);
    }

    /**
     * @param  \SimpleXMLElement            $response
     * @throws CommonApiValidationException
     */
    public function checkErrors(\SimpleXMLElement $response)
    {
        $this->errorBag = array_values((array) $response->Footer->Errors);

        if (count($this->errorBag)) {
            throw new CommonApiValidationException(
                'Rolls API has responded with the following errors: ' . \GuzzleHttp\json_encode($this->errorBag)
            );
        }
    }

}
