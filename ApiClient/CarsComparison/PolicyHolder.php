<?php

namespace App\Services\RollsClient\CarsComparison;

/**
 * This is a DTO class.
 * PolicyHolder is a owner of a related Car
 * @package App\Services\RollsClient
 */
class PolicyHolder
{

    /**
     * @var Car
     */
    private $car;

    /**
     * Zip code or postal code
     * @var string
     */
    private $zipCode;

    /**
     * @var \DateTimeImmutable
     */
    private $birthDate;

    /**
     * What age the policy holder was when he/she get his/her driver license.
     * @var int
     */
    private $driverLicenseRegistrationAge;

    /**
     * Years without car injury
     * @var int
     */
    private $damageFreeYears;

    /**
     * @param Car                $car
     * @param string             $zipCode
     * @param \DateTimeImmutable $birthDate
     * @param int                $driverLicenseRegistrationAge
     * @param int                $damageFreeYears
     */
    public function __construct(
        Car $car,
        string $zipCode,
        \DateTimeImmutable $birthDate,
        int $driverLicenseRegistrationAge,
        int $damageFreeYears = null
    )
    {
        $this->car = $car;
        $this->zipCode = str_replace(" ", "", $zipCode);
        $this->birthDate = $birthDate;
        $this->driverLicenseRegistrationAge = $driverLicenseRegistrationAge;
        $this->damageFreeYears = $damageFreeYears ?? $this->getDrivingExperienceYears();
    }

    /**
     * How much time the policy holder drives the car
     * (not this specific car, but his driving experience).
     * In years.
     *
     * @return int
     */
    public function getDrivingExperienceYears(): int
    {
        $age = (new \DateTimeImmutable)->diff($this->birthDate)->y;
        return $age - $this->driverLicenseRegistrationAge;
    }

    /**
     * @return Car
     */
    public function getCar(): Car
    {
        return $this->car;
    }

    /**
     * Zip code or postal code
     *
     * @return string
     */
    public function getZipCode(): string
    {
        return $this->zipCode;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getBirthDate(): \DateTimeImmutable
    {
        return $this->birthDate;
    }

    /**
     * Years without car injury
     *
     * @return int
     */
    public function getDamageFreeYears(): int
    {
        return $this->damageFreeYears;
    }

}