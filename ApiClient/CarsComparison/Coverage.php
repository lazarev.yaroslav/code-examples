<?php

namespace App\Services\RollsClient\CarsComparison;

/**
 * DTO, Enum class.
 *
 * Coverage (dekking). Allows to filter products by specific coverage.
 *
 * wa = Wettelijke Aansprakelijkheid, LEGAL_LIABILITY
 * bc = Beperkt Casco,                LIMITED_CASCO
 * vc = Volledig Casco,               COMPLETE_CASCO
 *
 * @package App\Services\RollsClient\CarsComparison
 */
class Coverage
{
    const COVERAGES = ['wa', 'bc', 'vc'];

    private $value;

    public function __construct(string $type)
    {
        if (! in_array($type, self::COVERAGES)) {
            throw new \InvalidArgumentException('Invalid Enum value. Possible values: ' . implode(', ', $values));
        }

        $this->value = $type;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
