<?php

namespace App\Services\RollsClient\CarsComparison;

use App\Services\RollsClient\CommonApiClient;

/**
 * Provides remote objective access to Rolls XML API.
 * Allows to get a list of insurance rates.
 *
 * @package App\Services\RollsClient\CarsComparison
 */
class RatesGateway
{

    /**
     * @var CommonApiClient
     */
    private $commonApiClient;

    /**
     * @param CommonApiClient $commonApiClient
     */
    public function __construct(CommonApiClient $commonApiClient)
    {
        $this->commonApiClient = $commonApiClient;
    }

    /**
     * Get rates from Rolls API and adapt to internal DTO
     *
     * @param  PolicyHolder $policyHolder
     * @param  int[]        $productIDs
     * @param  Coverage     $coverage             Coverage (dekking). Optional. Allows to filter products by specific coverage.
     * @return Rate[]
     */
    public function fetchRates(PolicyHolder $policyHolder, array $productIDs, Coverage $coverage): array
    {
        $response = $this->commonApiClient->send($this->getEnvelope($policyHolder, $productIDs, $coverage));

        $ratesXml = $response->Functie->Parameters->Premieobjecten->Premieobject->Premies;

        $rates = [];

        foreach($ratesXml->Premie as $rateXml){
            array_push($rates, $this->unserializeRate($rateXml));
        }

        return $rates;
    }

    /**
     * KS** means the codes of different Rolls API remote calls.
     *
     * KS305701 means performing an insurance rate calculation.
     * The supplementary insurance is excluded.
     *
     * @param  PolicyHolder $policyHolder
     * @param  int[]        $productIDs
     * @param  Coverage     $coverage             Coverage (dekking). Optional. Allows to filter products by specific coverage.
     * @return string                             XML Envelope
     */
    private function getEnvelope(PolicyHolder $policyHolder, array $productIDs, Coverage $coverage): string
    {
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>
                <Rollsenvelope>
                    <Header />
                    <Functie code=\"KS305701\">
                        <Parameters>
                            {$this->serializePolicyHolder($policyHolder)}
                            {$this->serializeCar($policyHolder)}
                            <Premieobjecten type=\"xml\" verplicht=\"ja\" direction=\"in/out\">
                                <Premieobject>
                                    <PO_Productselectie>
                                        {$this->serializeProducts($productIDs)}
                                    </PO_Productselectie>
                                    <Betalingstermijn>maand</Betalingstermijn>
                                    <Contractsduur>1</Contractsduur>
                                    <Dekking>{$coverage}</Dekking>
                                    <Gewensteigenrisico>-1</Gewensteigenrisico>
                                    <Assurantiebelastingincl>ja</Assurantiebelastingincl>
                                    <Poliskostenincl>nee</Poliskostenincl>
                                    <Premies />
                                </Premieobject>
                            </Premieobjecten>
                        </Parameters>
                    </Functie>
                    <Footer />
                </Rollsenvelope>";
    }

    /**
     * @param  PolicyHolder  $policyHolder
     * @return string
     */
    private function serializePolicyHolder(PolicyHolder $policyHolder): string
    {
        return "<Regelmatigebestuurder type=\"xml\" verplicht=\"ja\" direction=\"in\">
                    <Geboortedatum>{$policyHolder->getBirthDate()->format('Ymd')}</Geboortedatum>
                    <Postcode>{$policyHolder->getZipCode()}</Postcode>
                    <Geslacht>man</Geslacht>
                    <Jarenrijbewijs>{$policyHolder->getDrivingExperienceYears()}</Jarenrijbewijs>
                    <Jarenschadevrij>{$policyHolder->getDamageFreeYears()}</Jarenschadevrij>
                    <Jarenverzekerd>{$policyHolder->getDrivingExperienceYears()}</Jarenverzekerd>
                </Regelmatigebestuurder>
                <Verzekeringnemer type=\"xml\" verplicht=\"nee\" direction=\"in\">
                    <Postcode>{$policyHolder->getZipCode()}</Postcode>
                    <Natuurlijkpersoon>ja</Natuurlijkpersoon>
                </Verzekeringnemer>";
    }

    /**
     * @param  PolicyHolder $policyHolder
     * @return string
     */
    private function serializeCar(PolicyHolder $policyHolder): string
    {
        $tomorrow = strval(date("Ymd", time() + 24 * 60 * 60));

        return "<Voertuig type=\"xml\" verplicht=\"ja\" direction=\"in\">
                    <Inclbtw>ja</Inclbtw>
                    <Kenteken>{$policyHolder->getCar()->getLicensePlate()}</Kenteken>
                    <Meldcode>1234</Meldcode>
                    <Eersteeigenaar>nee</Eersteeigenaar>
                    <Waardeaccessoires>0</Waardeaccessoires>
                </Voertuig>
                <Nieuwepolis type=\"xml\" verplicht=\"ja\" direction=\"in\">
                    <Ingangsdatum>{$tomorrow}</Ingangsdatum>
                    <Particuliergebruik>ja</Particuliergebruik>
                    <Kilometrage>{$policyHolder->getCar()->getKilometrage()}</Kilometrage>
                    <Inclbtw>ja</Inclbtw>
                </Nieuwepolis>";
    }

    /**
     * @param  int[]   $productIDs
     * @return string
     */
    private function serializeProducts(array $productIDs): string
    {
        $serialized = '';

        foreach ($productIDs as $ID) {
            $serialized .= sprintf('<PO_Product><Id>%d</Id></PO_Product>', $ID);
        }

        return $serialized;
    }

    /**
     * @param  \SimpleXMLElement  $serialized
     * @return Rate
     */
    private function unserializeRate(\SimpleXMLElement $serialized): Rate
    {
        // it's important to convert types implicitly due to SimpleXML nature

        $product = new Product(
            intval($serialized->Productid),
            strval($serialized->Productnaam),
            strval($serialized->Maatschappijnaam),
            intval($serialized->Totaalscore),
            intval($serialized->Assurantiebelasting),
            intval($serialized->Assurantiebelastingpercentage)
        );

        return new Rate(
            intval($serialized->PremiebedragInCenten),
            intval($serialized->PoliskostenInCenten),
            $product,
            intval($serialized->Eigenrisico),
            $serialized->Acceptatie === 'nee' ? false : true
        );
    }

}
