<?php

namespace App\Services\RollsClient\CarsComparison;

/**
 * DTO class.
 * @package App\Services\RollsClient\CarsComparison
 */
class Product
{

    /**
     * Rolls ID of the product
     *
     * @var int
     */
    private $ID;

    /**
     * @var string
     */
    private $name;

    /**
     * Company name is different than product name.
     * It contains only the name of company, like OHRA,
     * against product name, that contains package name.
     *
     * aka Maatschappijnaam
     *
     * @var string
     */
    private $companyName;

    /**
     * @var int
     */
    private $rating;

    /**
     * aka Assurantiebelasting
     *
     * @var int
     */
    private $assuranceTax;

    /**
     * aka Assurantiebelastingpercentage
     *
     * @var int
     */
    private $assuranceTaxRate;

    /**
     * @param int    $ID
     * @param string $name
     * @param string $companyName
     * @param int    $rating
     * @param int    $assuranceTax
     * @param int    $assuranceTaxRate
     */
    public function __construct(
        int $ID,
        string $name,
        string $companyName,
        int $rating = 0,
        int $assuranceTax,
        int $assuranceTaxRate
    )
    {
        $this->ID = $ID;
        $this->name = $name;
        $this->companyName = $companyName;
        $this->rating = $rating;
        $this->assuranceTax = $assuranceTax;
        $this->assuranceTaxRate = $assuranceTaxRate;
    }

    /**
     * @return string
     */
    public function getCompanyName(): string
    {
        return $this->companyName;
    }

    /**
     * @return int
     */
    public function getAssuranceTax(): int
    {
        return $this->assuranceTax;
    }

    /**
     * @return int
     */
    public function getAssuranceTaxRate(): int
    {
        return $this->assuranceTaxRate;
    }

    /**
     * @return int
     */
    public function getID(): int
    {
        return $this->ID;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getRating(): int
    {
        return $this->rating;
    }

}