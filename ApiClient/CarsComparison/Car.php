<?php

namespace App\Services\RollsClient\CarsComparison;

/**
 * This is a DTO class.
 * @package App\Services\RollsClient
 */
class Car
{

    const DEFAULT_KILOMETRAGE = 12001;

    /**
     * Car number / license number
     *
     * @var string
     */
    private $licensePlate;

    /**
     * @var \DateTimeImmutable
     */
    private $productionDate;

    /**
     * Brand of the car
     *
     * @var string
     */
    private $brand;

    /**
     * Model of the car
     *
     * @var string
     */
    private $model;

    /**
     * @var string
     */
    private $description;

    /**
     * @var int
     */
    private $kilometrage;

    /**
     * @param string              $licensePlate
     * @param int                 $kilometrage
     * @param \DateTimeImmutable  $productionDate
     * @param string              $brand
     * @param string              $model
     * @param string              $description
     */
    public function __construct(
        string $licensePlate,
        int $kilometrage = self::DEFAULT_KILOMETRAGE,
        \DateTimeImmutable $productionDate = null,
        string $brand = null,
        string $model = null,
        string $description = null
    )
    {
        $this->licensePlate    = $licensePlate;
        $this->kilometrage     = $kilometrage;
        $this->productionDate  = $productionDate;
        $this->brand           = $brand;
        $this->model           = $model;
        $this->description     = $description;
    }

    /**
     * @return string
     */
    public function getLicensePlate(): string
    {
        return $this->licensePlate;
    }

    /**
     * @return int
     */
    public function getKilometrage(): int
    {
        return $this->kilometrage;
    }

    /**
     * @param int $kilometrage
     */
    public function setKilometrage(int $kilometrage)
    {
        $this->kilometrage = $kilometrage;
    }

    /**
     * @param  string    $licensePlate
     * @param  \stdClass $carResponse
     * @param  int       $kilometrage
     * @return Car
     */
    public static function createFromVehicleApi(
        string $licensePlate,
        \stdClass $carResponse,
        int $kilometrage = self::DEFAULT_KILOMETRAGE
    ): Car
    {
        $month = strlen($carResponse->ConstructionMonth) == 1 ?
            '0' . $carResponse->ConstructionMonth :
            $carResponse->ConstructionMonth;

        return new self(
            $licensePlate,
            $kilometrage,
            \DateTimeImmutable::createFromFormat('Ynd', $carResponse->ConstructionYear . $month . '01'),
            $carResponse->Make,
            $carResponse->Model,
            $carResponse->TypeDescriptionLong
        );
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return json_encode(get_object_vars($this));
    }

}