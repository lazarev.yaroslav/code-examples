<?php

namespace App\Services\RollsClient\CarsComparison;

use App\Services\RollsClient\VehicleApiClient;

/**
 * @package App\Services\RollsClient\CarsComparison
 */
class CarComparison
{

    /**
     * @var VehicleApiClient
     */
    private $vehicleApiClient;

    /**
     * @var RatesGateway
     */
    private $ratesGateway;

    /**
     * @param VehicleApiClient $vehicleApiClient
     * @param RatesGateway     $ratesGateway
     */
    public function __construct(VehicleApiClient $vehicleApiClient, RatesGateway $ratesGateway)
    {
        $this->vehicleApiClient = $vehicleApiClient;
        $this->ratesGateway = $ratesGateway;
    }

    /**
     * @param  string             $licensePlate
     * @param  int                $kilometrage
     * @return Car
     */
    public function findCar(string $licensePlate, $kilometrage = Car::DEFAULT_KILOMETRAGE): Car
    {
        $carResponse = $this->vehicleApiClient->findCar($licensePlate);
        $car = Car::createFromVehicleApi($licensePlate, $carResponse, $kilometrage);
        $car->setKilometrage($kilometrage);
        return $car;
    }

    /**
     * @param  PolicyHolder $policyHolder
     * @param  int[]        $productIDs           External product ID's by Rolls API
     * @param  Coverage     $coverage             Coverage (dekking). Optional. Allows to filter products by specific coverage.
     * @return Rate[]
     */
    public function getRates(PolicyHolder $policyHolder, array $productIDs, Coverage $coverage): array
    {
        return $this->ratesGateway->fetchRates($policyHolder, $productIDs, $coverage);
    }

}
