<?php

namespace App\Services\RollsClient\CarsComparison;

class Rate
{

    /**
     * Insurance cost in cents.
     *
     * @var int
     */
    private $cost;

    /**
     * Policy cost in cents.
     * Fixed price that an insurance company charge for a customer
     *
     * @var int
     */
    private $policyCost;

    /**
     * @var Product
     */
    private $product;

    /**
     * In Euros.
     * @var int
     */
    private $ownRisk;

    /**
     * @var bool
     */
    private $enabled;

    /**
     * @param int     $cost
     * @param int     $policyCost
     * @param Product $product
     * @param int     $ownRisk
     * @param bool    $enabled
     */
    public function __construct(int $cost, int $policyCost, Product $product, int $ownRisk, bool $enabled)
    {
        $this->cost = $cost;
        $this->policyCost = $policyCost;
        $this->product = $product;
        $this->ownRisk = $ownRisk;
        $this->enabled = $enabled;
    }

    /**
     * @return int
     */
    public function getCost(): int
    {
        return $this->cost;
    }

    /**
     * Policy cost in cents.
     * Fixed price that an insurance company charge for a customer
     *
     * @return int
     */
    public function getPolicyCost(): int
    {
        return $this->policyCost;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getOwnRisk(): int
    {
        return $this->ownRisk;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

}