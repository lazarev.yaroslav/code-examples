<?php

namespace App\Services\RollsClient;

use App\Services\RollsClient\Exceptions\VehicleApiClientException;

/**
 * @package App\Services\RollsClient
 */
class VehicleApiClient
{

    const BASE_URL = 'base_url';

    /**
     * @var string
     */
    private $clientID;

    /**
     * @var string
     */
    private $clientSecret;

    /**
     * @param string $clientID
     * @param string $clientSecret
     */
    public function __construct(string $clientID, string $clientSecret)
    {
        $this->clientID = $clientID;
        $this->clientSecret = $clientSecret;
    }

    /**
     *
     * @param  string                      $licensePlate
     * @return \stdClass
     * @throws VehicleApiClientException
     */
    public function findCar(string $licensePlate): \stdClass
    {
        $curl = curl_init(self::BASE_URL . '/licenseplates/basic/' . $licensePlate);

        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Accept-Language: nl-NL',
            'ClientID: ' . $this->clientID,
            'ClientSecret: ' . $this->clientSecret
        ]);

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        $response = json_decode($response);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new VehicleApiClientException('Response body is not a valid json.');
        }

        return $response;
    }

}
