<?php

namespace App\Http\Controllers;

use App\Http\Requests\CarRatesRequest;
use App\Presenters\CarRatesPresenter as RatesPresenter;
use App\Services\RollsClient\CarsComparison\CarComparison as ComparisonService;
use App\Services\RollsClient\CarsComparison\Car;
use App\Services\RollsClient\CarsComparison\PolicyHolder;
use App\Services\RollsClient\CarsComparison\Coverage;
use App\Services\RollsClient\CarsComparison\Rate;
use Illuminate\Http\Request;
use Illuminate\Cache\Repository as Cache;

class CarComparisonController
{

    /**
     * @var ComparisonService
     */
    private $comparisonService;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var RatesPresenter
     */
    private $ratesPresenter;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * @param  ComparisonService $comparisonService
     * @param  Request           $request
     * @param  RatesPresenter    $ratesPresenter
     * @param  Cache             $cache
     */
    public function __construct(
        ComparisonService $comparisonService,
        Request $request,
        RatesPresenter $ratesPresenter,
        Cache $cache
    )
    {
        $this->comparisonService = $comparisonService;
        $this->request = $request;
        $this->ratesPresenter = $ratesPresenter;
        $this->cache = $cache;
    }

    /**
     * Get information of specific car.
     *
     * Needed fields: (GET parameters) kilometrage - (mileage, but in km) (can be set later to the Car object).
     *
     * @param  string                                         $licensePlate
     * @return \App\Services\RollsClient\CarsComparison\Car
     */
    public function getCar(string $licensePlate)
    {
        if (! $car = $this->getCachedCarInfo($licensePlate)) {

            $car = $this->comparisonService->findCar(
                $licensePlate,
                $this->request->get('kilometrage', Car::DEFAULT_KILOMETRAGE)
            );

            $this->cacheCarInfo($car);
        }

        return $car;
    }

    /**
     * Get the insurance rates of specific car for further comparing.
     *
     * @param  string          $licensePlate
     * @param  CarRatesRequest $request
     * @return string
     */
    public function getRates(string $licensePlate, CarRatesRequest $request)
    {
        if (! $rates = $this->getCachedRates($licensePlate)) {

            list($km, $zip, $bDate, $licenseY, $dmgFreeY, $coverage) = array_values($this->getRatesRequestParams());

            $car = $this->getCachedCarInfo($licensePlate) ?? $this->comparisonService->findCar($licensePlate, $km);

            $ph = new PolicyHolder($car, $zip, new \DateTimeImmutable($bDate), $licenseY, $dmgFreeY);

            $rates = $this->comparisonService->getRates($ph, $this->request->get('product_ids'), new Coverage($coverage));

            $this->cacheRates($licensePlate, $rates);

            $rates = $this->ratesPresenter->present($rates);
        }

        return $rates;
    }

    /**
     * @param  string $licensePlate
     * @return string                    Serialized car info, ready for response
     */
    private function getCachedCarInfo(string $licensePlate)
    {
        return $this->cache->get('car.info.'.$licensePlate);
    }

    /**
     * @param Car    $car
     */
    private function cacheCarInfo(Car $car)
    {
        $sixMonthAgo = (new \DateTime)->add(\DateInterval::createFromDateString('6 month'));

        $this->cache->add('car.info.'.$car->getLicensePlate(), $car, $sixMonthAgo);
    }

    /**
     * @param  string       $licensePlate
     * @return string|null                      Serialized rates.
     */
    private function getCachedRates(string $licensePlate)
    {
        return $this->cache->get(
            $this->getRatesCacheKey($licensePlate)
        );
    }

    /**
     * @param $licensePlate
     * @param Rate[]          $rates
     */
    private function cacheRates($licensePlate, array $rates)
    {
        $this->cache->add(
            $this->getRatesCacheKey($licensePlate),
            $this->ratesPresenter->present($rates),
            60 * 60 * 24
        );
    }

    /**
     * @return array
     */
    private function getRatesRequestParams(): array
    {
        return $this->request->only([
            'kilometrage',
            'zip_code',
            'birth_date',
            'license_age_years',
            'damage_free_years',
            'coverage'
        ]);
    }

    private function getRatesCacheKey($licensePlate): string
    {
        return 'car.rates.' . $licensePlate . http_build_query($this->getRatesRequestParams());
    }

}
