import axios from 'axios'

export default class HttpAdapter {
    /**
     * @returns {void}
     * @param {string} url
     * @param {any} successCallback
     * @param {any} errorCallback
     */
    public static get(url: string, successCallback: (success) => any, errorCallback: (error) => any): void {
        axios.get(url).then(successCallback).catch(errorCallback)
    }

    /**
     * @returns {void}
     * @param {string} url
     * @param {object} data
     * @param {any} successCallback
     * @param {any} errorCallback
     */
    public static post(url: string, data: object, successCallback: (success) => any, errorCallback: (error) => any): void {
        axios.post(url, data).then(successCallback).catch(errorCallback)
    }

    /**
     * @returns {void}
     * @param {string} url
     * @param {object} data
     * @param {any} successCallback
     * @param {any} errorCallback
     */
    public static put(url: string, data: object, successCallback: (success) => any, errorCallback: (error) => any): void {
        axios.put(url, data).then(successCallback).catch(errorCallback)
    }

    /**
     * @returns {void}
     * @param {string} url
     * @param {number} id
     * @param {any} successCallback
     * @param {any} errorCallback
     */
    public static delete(url: string, id: number, successCallback: (success) => any, errorCallback: (error) => any): void {
        axios.delete(`${url}/${id}`).then(successCallback).catch(errorCallback)
    }
}