import NotificationInterface from '../interfaces/common/notification/NotificationInterface'

/**
 * import vue instance
 */
import { app } from '../app'

/**
 * Notification Class based on vue-toastr plagin:
 * https://github.com/s4l1h/vue-toastr
 * All options which can be used if you create object or call the method:
 * https://github.com/s4l1h/vue-toastr#new-toast-message-with-options
 */
export default class Notification implements NotificationInterface {
    private app
    public commonOptions

    /**
     *
     * @param {any} options
     * @param {object} app
     */
    public constructor(options = null) {
        this.app = app

        let defaultCommonOptions = {
            clickClose: true,
            timeout: 2000,
            progressbar: true,
            position: 'toast-top-right',
            preventDuplicates: false,
        }

        this.commonOptions = Object.assign(defaultCommonOptions, options)
    }

    /**
     *
     * @param {string} msg
     * @param {object | null} options
     * @return {void}
     */
    public success(msg: string = 'Success', options: object | null = null): void {
        let defaultOptions = Object.assign(
            {
                title: 'Success',
                msg: msg,
                type: 'success',
            },
            this.commonOptions
        )
        options = Object.assign(defaultOptions, options)
        this.app.$toastr.Add(options)
    }

    /**
     *
     * @param {string} msg
     * @param {object | null} options
     * @return {void}
     */
    public error(msg = 'Error', options: object | null = null): void {
        let defaultOptions = Object.assign(
            {
                title: 'Error',
                msg: msg,
                type: 'error',
            },
            this.commonOptions
        )

        options = Object.assign(defaultOptions, options)
        this.app.$toastr.Add(options)
    }

    /**
     *
     * @param {string} msg
     * @param {object | null} options
     * @return {void}
     */
    public warning(msg = 'Are you sure?', options: object | null = null): void {
        let defaultOptions = Object.assign(
            {
                title: 'Warning',
                msg: msg,
                type: 'warning',
            },
            this.commonOptions
        )

        options = Object.assign(defaultOptions, options)
        this.app.$toastr.Add(options)
    }
}