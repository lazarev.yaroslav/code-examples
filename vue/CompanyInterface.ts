/**
 * List to present
 */
export interface ListCompanyInterface {
    id: number,
    logo: string,
    name: string,
    policy_cost: number|string,
    description: string,
}

/**
 * Data on back-end
 */
export interface CompanyInterface {
    id: number,
    name: string,
    logo_url: string,
    poliskosten: number,
    description: string
}

export interface StoreCompanyInterface {
    id: number|null,
    name: string,
    logo_url: string,
    poliskosten: number|string,
    description: string
}