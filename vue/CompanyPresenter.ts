import { ListCompanyInterface, CompanyInterface, StoreCompanyInterface } from '../../interfaces/companies/CompanyInterface'

export default class CompanyPresenter {
    /**
     * @param {Array<CompanyInterface>} items
     * @returns {ListCompanyInterface[]}
     */
    public get(items: Array<CompanyInterface>): ListCompanyInterface[] {
        return items.map((item) => {
            return {
                id: item.id,
                logo: item.logo_url,
                name: item.name,
                policy_cost: item.poliskosten,
                description: item.description
            }
        })
    }

    /**
     *
     * @param {ListCompanyInterface} item
     * @return {StoreCompanyInterface}
     */
    public store(item: ListCompanyInterface): StoreCompanyInterface {
        return {
            id: item.id,
            name: item.name,
            logo_url: item.logo,
            poliskosten: item.policy_cost,
            description: item.description
        }
    }

    /**
     *
     * @returns {StoreCompanyInterface}
     */
    public initCompany(): StoreCompanyInterface {
        return {
            id: null,
            name: '',
            logo_url: '',
            poliskosten: '',
            description: ''
        }
    }
}