import Vue from 'vue'
import Component from 'vue-class-component'
import HttpAdapter from '../../../adapters/HttpAdapter'
import NotificationAdapter from '../../../adapters/NotificationAdapter'
import { Prop } from 'vue-property-decorator'

@Component
export default class BaseCrudBtn extends Vue {
    protected http = HttpAdapter
    protected alert = new NotificationAdapter
    protected loading: boolean = false
    protected isModalOpen: boolean = false

    @Prop({ required: true }) protected route!: string

    public showModal() {
        this.isModalOpen = true
    }

    public closeModal() {
        this.isModalOpen = false
    }
}

